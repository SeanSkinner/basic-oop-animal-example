package no.noroff.sean.animal;

public interface Cannibal {
    void eatOwnKind();
}
