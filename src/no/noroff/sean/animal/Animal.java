package no.noroff.sean.animal;

public abstract class Animal implements Eat {
    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public abstract void move();

    public void roar() {
        System.out.println("Very generic roar");
    }
}
