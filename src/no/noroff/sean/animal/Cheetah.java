package no.noroff.sean.animal;

public class Cheetah extends Animal {
    public Cheetah(String name, int age) {
        super(name, age);
    }

    @Override
    public void move() {
        System.out.println("The cheetah runs really fast!");
    }

    @Override
    public void roar() {
        System.out.println("Very scary cheetah roar!");
    }

    @Override
    public void eat() {
        System.out.println("The cheetah snacks on a tasty antelope");
    }
}
