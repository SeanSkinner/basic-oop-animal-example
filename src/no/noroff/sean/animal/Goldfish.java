package no.noroff.sean.animal;

public class Goldfish extends Animal implements CircleSwim, Cannibal {
    private int numberOfFins;

    public Goldfish(String name, int age, int numberOfFins) {
        super(name, age);
        this.numberOfFins = numberOfFins;
    }

    @Override
    public void move() {
        System.out.println("The goldfish swims around the lake");
    }

    public void swimFast() {
        System.out.println("The goldfish swims swiftly away from a scary predator");
    }

    @Override
    public void eat() {
        System.out.println("The goldfish nibbles on some tasty plankton");
    }


    @Override
    public void eatOwnKind() {

    }

    @Override
    public void swimInCircle() {

    }
}
