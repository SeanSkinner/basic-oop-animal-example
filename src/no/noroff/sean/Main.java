package no.noroff.sean;

import no.noroff.sean.animal.Animal;
import no.noroff.sean.animal.Cheetah;
import no.noroff.sean.animal.Goldfish;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Animal> myZoo = new ArrayList<Animal>();

        Cheetah myCheetah = new Cheetah("Zeus", 2);
        Goldfish myGoldfish = new Goldfish("Marvin", 1, 2);

        myZoo.add(myCheetah);
        myZoo.add(myGoldfish);

        for (Animal animal:myZoo) {
            animal.roar();
            animal.eat();
        }
    }
}
